<?php
if(isset($_GET['pesan'])){
  if($_GET['pesan']=="berhasil"){
    echo "<div class='alert alert-success'>Registrasi Berhasil !</div>";
  }
} ?>
<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data registrasi</h6>
        <a href="#" class="btn btn-primary mt-2">Tambah Data</a>
    </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Level</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <?php $no = 1;
                         $regs = $regObj->displayUser();
                         foreach ($regs as $reg){
                     ?>
                    <tbody>
                        <tr>
                            <td><?= $reg['nama']; ?></td>
                            <td><?= $reg['level']; ?></td>
                            <td>
                            <a href="#" class="btn btn-primary " data-toggle="modal" data-target="#show1"
                            data-id="<?= $reg['id']?>">detail</a>
                            </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  </div>
<!-- /.container-fluid -->
<!-- Modal Ubah -->

<div class="modal fade" id="show1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Detail</b></h4>
                </div>
                <div class="modal-body">
                    <div class="modal-data">

                    </div>
                </div>
            </div>
      </div>
</div>
<!-- End of Modal -->
<script type="text/javascript">

    $(document).ready(function(){

        $('#show').on('show.bs.modal', function (e) {

            var idx = $(e.relatedTarget).data('id');

            //menggunakan fungsi ajax untuk pengambilan data

            $.ajax({

                type : 'post',

                url : 'detailUser.php',

                data :  'idx='+ idx,

                success : function(data){

                $('.modal-data').html(data);//menampilkan data ke dalam modal

                }

            });

         });

    });

  </script>
