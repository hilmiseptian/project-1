<?php
  // Include database file
  include 'admin.php';

  $regObj = new Admin();
    // Edit customer record
    if(isset($_POST['idx']) && !empty($_POST['idx'])) {
      $id = $_POST['idx'];
      $reg = $regObj->displayRegistrasiById($id);
?>
  <!-- Modal -->
  <form method="post" action="">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group row">
            <label for="nama" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-md-12">
              <input type="text" class="form-control" id="nama" name="nama" value="<?= $reg['nama'] ?>">
              <input hidden type="number" class="form-control" id="od_reg" name="id_reg" value="<?= $reg['id_reg'] ?>">
            </div>
          </div>
          <div class="form-group row">
            <label for="kelas" class="col-sm-2 col-form-label">Kelas</label>
            <div class="col-md-12">
              <select class="form-control" name="id_kelas">
                <option value="<?php $reg['id_kelas'] ?>"> <?= $reg['nama_kelas'] ?> </option>
                <option value="1">XII - Ipa</option>
                <option value="2">XII - Ips</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
            <div class="col-md-12">
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="alamat" ><?= $reg['alamat'] ?></textarea>
            </div>
          </div>
          <div class="form-group row">
            <label for="jk" class="col-sm-2 col-form-label">Jenis Kelamin</label>
            <div class="col-md-12">
              <select class="form-control" name="jk">
                <option value="<?php $reg['jk'] ?>"><?= $reg['jk'] ?></option>
                <option value="laki-laki">Laki - laki</option>
                <option value="perempuan">Perempuan</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-md-12">
              <input type="email" class="form-control" id="inputEmail3" name="email" value="<?= $reg['email'] ?>">
              <input hidden type="text" name="pass" value="<?php $reg['pass'] ?>">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-12">
              <button type="submit" name="update" class="btn btn-success">Konfirm</button>
              <button type="submit" name="submit" class="btn btn-warning">Kembali</button>
            </div>
          <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </form>
