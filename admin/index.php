	<?php
		include '../template/header.php';
		include 'admin.php';
  	$regObj = new Admin();
		// cek apakah yang mengakses halaman ini sudah login
		if($_SESSION['level']==""){
			header("location:index.php?pesan=gagal");

		}
				// Update Record in customer table
		if(isset($_POST['update'])) {
			$regObj->konfirmasiRecord($_POST);
		}
		?>

    <?php
    // yow
    if(isset($_GET['page'])){
      $page = $_GET['page'];

      switch ($page) {
        case 'dashboard':
          include "dashboard.php";
          break;
        case 'profil':
          include "profil.php";
          break;
        case 'user':
          include "user.php";
          break;
        case 'registrasi':
          include "registrasi.php";
          break;
        default:
          echo "<center><h3>Maaf. Halaman tidak di temukan !</h3></center>";
          break;
      }
    }else{
      include "beranda.php";
    }

     ?>

	<?php include '../template/footer.php'; ?>
	<!-- End of Modal -->
	<script type="text/javascript">

	    $(document).ready(function(){

	        $('#show').on('show.bs.modal', function (e) {

	            var idx = $(e.relatedTarget).data('id');

	            //menggunakan fungsi ajax untuk pengambilan data

	            $.ajax({

	                type : 'post',

	                url : 'detailReg.php',

	                data :  'idx='+ idx,

	                success : function(data){

	                $('.modal-data').html(data);//menampilkan data ke dalam modal

	                }

	            });

	         });
	        $('#show1').on('show.bs.modal', function (e) {

	            var idx = $(e.relatedTarget).data('id');

	            //menggunakan fungsi ajax untuk pengambilan data

	            $.ajax({

	                type : 'post',

	                url : 'detailUser.php',

	                data :  'idx='+ idx,

	                success : function(data){

	                $('.modal-data').html(data);//menampilkan data ke dalam modal

	                }

	            });

	         });

	    });

	  </script>
