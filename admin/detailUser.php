<?php
  // Include database file
  include 'admin.php';

  $regObj = new Admin();

    // Edit customer record
    if(isset($_POST['idx']) && !empty($_POST['idx'])) {
      $id = $_POST['idx'];
      $reg = $regObj->displayUserById($id);
?>
  <!-- Modal -->
  <form method="post" action="">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group row">
            <label for="nama" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-md-12">
              <input type="text" class="form-control" id="nama" name="nama" value="<?= $reg['nama'] ?>">
              <input hidden type="number" class="form-control" id="id_reg" name="id_reg" value="<?= $reg['id'] ?>">
            </div>
          </div>
          <div class="form-group row">
            <label for="kelas" class="col-sm-2 col-form-label">User</label>
            <div class="col-md-12">
              <input type="text" class="form-control" id="nama" name="nama" value="<?= $reg['user'] ?>">
            </div>
          </div>
          <div class="form-group row">
            <label for="kelas" class="col-sm-2 col-form-label">Level</label>
            <div class="col-md-12">
              <input type="text" class="form-control" id="nama" name="nama" value="<?= $reg['level'] ?>">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-12">
              <button type="submit" name="update" class="btn btn-success">Konfirm</button>
              <button type="submit" name="submit" class="btn btn-warning">Kembali</button>
            </div>
          <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </form>
