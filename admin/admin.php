<?php

	class Admin
	{
		private $servername = "localhost";
		private $username   = "root";
		private $password   = "";
		private $database   = "db";
		public  $con;

		// Database Connection
		public function __construct()
		{
		    $this->con = new mysqli($this->servername, $this->username,$this->password,$this->database);
		    if(mysqli_connect_error()) {
			 trigger_error("Failed to connect to MySQL: " . mysqli_connect_error());
		    }else{
			return $this->con;
		    }
		}

		// Insert customer data into customer table
		public function insertData($post)
		{
			$name = $this->con->real_escape_string($_POST['nama_produk']);
			$keterangan = $this->con->real_escape_string($_POST['keterangan']);
			$harga = $this->con->real_escape_string($_POST['harga']);
			$jumlah = $this->con->real_escape_string($_POST['jumlah']);
			$query="INSERT INTO produk (nama_produk,keterangan,harga,jumlah) VALUES('$name','$keterangan','$harga','$jumlah')";
			$sql = $this->con->query($query);
				if ($sql==true) {
							header("Location:index.php");
					}else{
					    echo "Produk Gagal Disimpan!";
					}
		}
		// Fetch customer records for show listing
		public function displayProfil()
		{
				$query = "SELECT * FROM user WHERE user = AND pass =";
				$result = $this->con->query($query);
		if ($result->num_rows > 0) {
				$data = array();
				while ($row = $result->fetch_assoc()) {
							 $data[] = $row;
				}
			 return $data;
				}else{
			 echo "Tidak Ditemukan Data";
				}
		}
		// Fetch customer records for show listing
		public function displayUser()
		{
				$query = "SELECT * FROM user";
				$result = $this->con->query($query);
		if ($result->num_rows > 0) {
				$data = array();
				while ($row = $result->fetch_assoc()) {
							 $data[] = $row;
				}
			 return $data;
				}else{
			 echo "Tidak Ditemukan Data";
				}
		}
		// Fetch single data for edit from customer table
		public function displayUserById($id)
		{
				$query ="SELECT * FROM user WHERE id  = '$id'";
				$result = $this->con->query($query);
		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			return $row;
				}else{
			echo "Tidak Ditemukan Data";
				}
		}
		// Fetch single data for edit from customer table
		public function displayRegistrasiById($id)
		{
				$query ="SELECT registrasi.id_reg,registrasi.email, registrasi.nama,registrasi.id_kelas,registrasi.alamat,registrasi.jk,registrasi.pass,registrasi.konfirmasi, kelas_tb.id,kelas_tb.nama_kelas FROM registrasi INNER JOIN kelas_tb ON registrasi.id_kelas = kelas_tb.id WHERE registrasi.id_reg = '$id'";
				$result = $this->con->query($query);
		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			return $row;
				}else{
			echo "Tidak Ditemukan Data";
				}
		}
		// Fetch customer records for show listing
		public function displayData()
		{
		    $query = "SELECT registrasi.id_reg,registrasi.nama,registrasi.id_kelas,registrasi.alamat,registrasi.jk,registrasi.konfirmasi, kelas_tb.id,kelas_tb.nama_kelas FROM registrasi INNER JOIN kelas_tb ON registrasi.id_kelas = kelas_tb.id where registrasi.konfirmasi = 'belum' order by registrasi.id_reg";
		    $result = $this->con->query($query);
		if ($result->num_rows > 0) {
		    $data = array();
		    while ($row = $result->fetch_assoc()) {
		           $data[] = $row;
		    }
			 return $data;
		    }else{
			 echo "Tidak Ditemukan Data";
		    }
		}

		// Fetch single data for edit from customer table
		public function displayRecordById($id)
		{
		    $query = "SELECT * FROM registrasi WHERE id = '$id'";
		    $result = $this->con->query($query);
		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			return $row;
		    }else{
			echo "Tidak Ditemukan Data";
		    }
		}


		// Update customer data into customer table
		public function konfirmasiRecord($postData)
		{
				$id = $this->con->real_escape_string($_POST['id_reg']);
				$nama = $this->con->real_escape_string($_POST['nama']);
				$id_kelas = $this->con->real_escape_string($_POST['id_kelas']);
				$alamat = $this->con->real_escape_string($_POST['alamat']);
				$jk = $this->con->real_escape_string($_POST['jk']);
				$email = $this->con->real_escape_string($_POST['email']);
				$pass = $this->con->real_escape_string($_POST['pass']);
				$query = "INSERT INTO murid (nama,id_kelas,alamat,jk,email,pass) VALUES('$nama','$id_kelas','$alamat','$jk','$email','$pass')";
				$sql = $this->con->query($query);
		if (!empty($id) && !empty($postData)) {
				// jalankan query UPDATE berdasarkan ID yang produknya kita edit
			 	$query  = "UPDATE registrasi SET konfirmasi = 'sudah'";
				$query .= "WHERE id_reg = '$id'";
				$result = mysqli_query($this->con, $query);
				$sql = $this->con->query($query);
				// periska query apakah ada error
				if ($sql==true) {
							header("Location:registrasi.php?pesam=berhaasil");
					}else{
							echo "Data Gagal Diubah";
					}
				}

		}
		// Update customer data into customer table
		public function updateRecord($postData)
		{
				$id = $this->con->real_escape_string($_POST['id']);
		    $nama = $this->con->real_escape_string($_POST['nama_produk']);
		    $keterangan = $this->con->real_escape_string($_POST['keterangan']);
		    $harga = $this->con->real_escape_string($_POST['harga']);
		    $jumlah = $this->con->real_escape_string($_POST['jumlah']);
		if (!empty($id) && !empty($postData)) {
        // jalankan query UPDATE berdasarkan ID yang produknya kita edit
       $query  = "UPDATE produk SET nama_produk = '$nama', keterangan = '$keterangan', harga = '$harga', jumlah = '$jumlah'";
        $query .= "WHERE id = '$id'";
        $result = mysqli_query($this->con, $query);
				$sql = $this->con->query($query);
        // periska query apakah ada error
				if ($sql==true) {
							header("Location:index.php");
					}else{
					    echo "Data Gagal Berhasil Diubah";
					}
		    }
			}
		// Delete customer data from customer table
		public function deleteRecord($id)
		{
		    $query = "DELETE FROM produk WHERE id = '$id'";
		    $sql = $this->con->query($query);
		if ($sql==true) {
			header("Location:index.php");
		}else{
			echo "Tidak Ditemukan Data";
		    }
		}

	}
?>
