<?php
if(isset($_GET['pesan'])){
  if($_GET['pesan']=="berhasil"){
    echo "<div class='alert alert-success'>Registrasi Berhasil !</div>";
  }
} ?>
<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data registrasi</h6>
    </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Alamat</th>
                            <th>Jenis - kelamin</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <?php $no = 1;
                         $regs = $regObj->displayData();
                         foreach ($regs as $reg){
                     ?>
                    <tbody>
                        <tr>
                            <td><?= $reg['nama']; ?></td>
                            <td><?= $reg['nama_kelas']; ?></td>
                            <td><?= $reg['alamat']; ?></td>
                            <td><?= $reg['jk']; ?></td>
                            <td>
                            <a href="#" class="btn btn-primary " data-toggle="modal" data-target="#show"
                            data-id="<?= $reg['id_reg']?>">detail</a>
                            </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  </div>
<!-- /.container-fluid -->
<!-- Modal Ubah -->

<div class="modal fade" id="show" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Detail</b></h4>
                </div>
                <div class="modal-body">
                    <div class="modal-data">

                    </div>
                </div>
            </div>
      </div>
</div>
<!-- End of Modal -->
