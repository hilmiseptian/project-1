<?php include '../template/header.php'; ?>

		<?php
		//session_start();

		// cek apakah yang mengakses halaman ini sudah login
		if($_SESSION['level']==""){
			header("location:index.php?pesan=gagal");
		}

		?>

    <?php
    // yow
    if(isset($_GET['page'])){
      $page = $_GET['page'];

      switch ($page) {
        case 'dashboard':
          include "dashboard.php";
          break;
        case 'profil':
          include "profil.php";
          break;
        case 'user':
          include "user.php";
          break;
        case 'registrasi':
          include "registrasi.php";
          break;
        case 'logout':
          include "logout.php";
          break;
        default:
          echo "<center><h3>Maaf. Halaman tidak di temukan !</h3></center>";
          break;
      }
    }else{
      include "beranda.php";
    }

     ?>

	<?php include '../template/footer.php'; ?>
