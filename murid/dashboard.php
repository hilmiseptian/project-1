<!doctype html>
<html lang="en" id="home">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../../asset/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="../../asset/css/style.css">
    <link href="../../../asset/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
      href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
      rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../../asset/css/sb-admin-2.min.css" rel="stylesheet">
    <title>My Portofolio!</title>
		<style media="screen">
		 .profil .img-top {
    width: 150px;
    height: 150px;
    border:  5px solid #666;
    box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.5);
    }
		</style>
  </head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-gradient-info">
	  <a class="navbar-brand" href="#">
      <img src="../../../asset/img/logo.png" width="50" height="50" class="d-inline-block align-top" alt="">
    </a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
	    <div class="navbar-nav">
	      <a class="nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
	      <a class="nav-link" href="#">Features</a>
	      <a class="nav-link" href="#">Pricing</a>
	    </div>
	  </div>
	</nav>


<!-- pembatas -->
<div class="container-fluid">

  <div class="row">
    <!-- profil -->
    <div class="col-xl-2 card ml-3 mt-3 d-flex align-items-center justify-content-center bg-light border-bottom-secondary" style="width: 18rem;" id="profil">
      <img src="../../../asset/img/profil.jpg" class="img-top rounded-circle mt-3" style="width: 150px; height: 150px; border:  5px solid #666;
        box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.5);">
        <div class="card-body">
          <h5 class="card-title" style="text-align: center">Hilmi Saftian</h5>
          <a href="#" class="card-link">Pengaturan</a>
          <a href="../../logout.php" class="card-link">Keluar</a>
        </div>
        <div class="card-body">

        </div>
      </div>

      <!-- Earnings (Monthly) Card Example -->
      <div class="col-md-2 mb-4 mt-3" style="height : 150px">
          <div class="card border-left-primary shadow h-100 py-2">
              <div class="card-body">
                  <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                          <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                              <a href="#">Kelas Saya</a></div>
                          <div class="h5 mb-0 font-weight-bold text-gray-800">$40,000</div>
                      </div>
                      <div class="col-auto">
                          <i class="fas fa-calendar fa-2x text-gray-300"></i>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <!-- Earnings (Annual) Card Example -->
      <div class="col-md-2 mb-4 mt-3" style="height : 150px">
          <div class="card border-left-success shadow h-100 py-2">
              <div class="card-body">
                  <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                          <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                              Materi</div>
                          <div class="h5 mb-0 font-weight-bold text-gray-800">$215,000</div>
                      </div>
                      <div class="col-auto">
                          <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <!-- Tasks Card Example -->
      <div class=" col-md-2 mb-4 mt-3" style="height : 150px">
          <div class="card border-left-info shadow h-100 py-2">
              <div class="card-body">
                  <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                          <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Pelanggaran
                          </div>
                          <div class="row no-gutters align-items-center">
                              <div class="col-auto">
                                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
                              </div>

                          </div>
                      </div>
                      <div class="col-auto">
                          <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <!-- pengumuman -->
      <div class="card mt-3 offset-md-1" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">Pengumuman <i class="fas fa-volume-up rotate-n-15"></i></h5>
          <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
          <a href="#" class="btn btn-primary">Go somewhere</a>
        </div>
      </div>
  </div>
</div>
</body>
