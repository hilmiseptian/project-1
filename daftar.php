<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SMA Negeri 8 Pandeglang</title>

    <!-- Custom fonts for this template-->
    <link href="asset/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="asset/css/sb-admin-2.min.css" rel="stylesheet">
    <!-- Custom styles for this page -->
    <link href="asset/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <style media="screen">
      body{
    padding:100px 0;
    background-color:#efefef
    }
    a, a:hover{
    color:#333
    }
    </style>
</head>
<body>
  <?php
  include 'murid/murid.php';
  $registrasiObj = new Murid();
    // Insert Record in registrasi table
    if(isset($_POST['submit'])) {
      $registrasiObj->register($_POST);
    }

    if(isset($_GET['pesan'])){
      if($_GET['pesan']=="berhasil"){
        echo "<div class='alert alert-success'>Registrasi Berhasil !</div>";
      }
    }

     ?>

  <!-- Image and text -->
<nav class="navbar navbar-light bg-light col-md-6 offset-3">
  <div class="container">
    <div class="row">
      <div class="">
        <a class="navbar-brand" href="#">
          <img src="asset/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
          Bootstrap
        </a>
      </div>
    </div>
  </div>
</nav>
  <form method="post" action="">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 offset-3 mt-3">
          <div class="form-group row">
            <label for="nama" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="nama" name="nama">
            </div>
          </div>
          <div class="form-group row">
            <label for="kelas" class="col-sm-2 col-form-label">Kelas</label>
            <div class="col-sm-10">
              <select class="form-control" name="id_kelas">
                <option value=""> - </option>
                <option value="1">XII - Ipa</option>
                <option value="2">XII - Ips</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
            <div class="col-sm-10">
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="alamat"></textarea>
            </div>
          </div>
          <div class="form-group row">
            <label for="jk" class="col-sm-2 col-form-label">Jenis Kelamin</label>
            <div class="col-sm-10">
              <select class="form-control" name="jk">
                <option value="laki-laki">-</option>
                <option value="laki-laki">Laki - laki</option>
                <option value="perempuan">Perempuan</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" id="inputEmail3" name="email">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
              <div class="input-group" id="show_hide_password">
                <input class="form-control" type="password" name="pass">
                <div class="input-group-addon">
                  <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-10">
              <button type="submit" name="submit" class="btn btn-primary">Sign in</button>
            </div>

          </div>
        </div>
      </div>
    </div>
  </form>

</body>
<script src="asset/jquery/jquery.min.js"></script>
<script src="asset/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="asset/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="asset/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="asset/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="asset/js/demo/chart-area-demo.js"></script>
<script src="js/demo/chart-pie-demo.js"></script>
<!-- Page level plugins -->
<script src="asset/datatables/jquery.dataTables.min.js"></script>
<script src="asset/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script src="asset/js/demo/datatables-demo.js"></script>
<script src="asset/js/script.js"></script>

</html>
