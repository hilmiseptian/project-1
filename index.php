
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>SMA Negeri 3 Pandeglang</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="asset/css/signin.css" rel="stylesheet">
  </head>
		<?php
		if(isset($_GET['pesan'])){
			if($_GET['pesan']=="gagal"){
				echo "<div class='alert'>Username dan Password tidak sesuai !</div>";
			}
		}
		?>
  <body class="text-center">
    <form action="cek_login.php?act=login" method="post" class="form-signin">
      <img class="mb-4" src="asset/img
      /logo.png" alt="" width="100" height="100">
      <h1 class="h3 mb-3 font-weight-normal">Silahkan Masuk</h1>
      <label for="inputEmail" class="sr-only">Nama</label>
      <input type="text" id="user" name="user" class="form-control" placeholder="Email " required autofocus>
      <label for="inputPassword" class="sr-only">Sandi</label>
      <input type="password" id="pass" name="pass" class="form-control" placeholder="Sandi" required>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Ingat saya
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit" value="LOGIN">Masuk</button>
      <a href="daftar.php" class="mt-3">Belum punya akun?</a>
      <p class="mt-5 mb-3 text-muted">&copy; 2020-2021</p>
    </form>
  </body>
</html>
